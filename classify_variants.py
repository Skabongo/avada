#! /usr/bin/env python3
#from __future__ import print_function

import sys
import os
from collections import defaultdict
import pickle
import sqlite3 as lite

from text.text_database import ReadDatabase
import util
import __settings__
from variant_classifier_features import create_feature_groups, get_pdf_positions, get_gene_names, flatten, create_gposs_to_eid, load_pmid_eid_to_right_gene_score


def load_clf(classifier_pkl_filename):
    with open(os.path.join(os.path.dirname(__file__), classifier_pkl_filename), 'rb') as f:
        return pickle.load(f)


def classify(classifier_pkl_filename, orig_str_to_vposs,
             orig_str_to_eids, eids_to_gposs, pmid):
    orig_str_to_proba_comment_eid = defaultdict(lambda: [])
    for orig_str, vposs in orig_str_to_vposs.items():
        print("handling orig str %s with %d vposs" % (orig_str, len(vposs)), file=sys.stderr)
        sys.stderr.flush()
        eids = set(orig_str_to_eids[orig_str])  # uniqify for safety ...
        if len(vposs) == 0:
            for eid in eids:
                orig_str_to_proba_comment_eid[orig_str].append((0.0, "missing_variant_position", eid))

        if not classify.clf:
            # print("loading classifier", file=sys.stderr)
            sys.stderr.flush()
            classify.clf = load_clf(classifier_pkl_filename=classifier_pkl_filename)

        # all_gposs = set(flatten(eids_to_gposs[eid] for eid in eids))
        all_gposs = create_gposs_to_eid(eids_to_gposs, eids)
        # print("have %d all_gposs for orig_str %s" % (len(all_gposs), orig_str), file=sys.stderr)
        sys.stderr.flush()
        for eid in eids:
            # print("for orig str %s, handling eid %s" % (orig_str, eid), file=sys.stderr)
            sys.stderr.flush()
            array = []
            # eid_gposs = set(eids_to_gposs[eid])
            eid_gposs = create_gposs_to_eid(eids_to_gposs, [eid])
            # print("have %d eid_gposs for orig_str %s" % (len(eid_gposs), orig_str), file=sys.stderr)
            sys.stderr.flush()
            if len(vposs) >= 100:
                orig_str_to_proba_comment_eid[orig_str].append((1.0, "gt100_vposs", eid))
            else:
                for vpos in vposs:
                    # print("Have variant %s at position %s" % (orig_str, str(vpos)), file=sys.stderr)
                    for feature_names, fg in create_feature_groups(vpos, eid_gposs, all_gposs, pmid, eid):
                        # print("variant %s, eid %s: %s" % (orig_str, eid, ','.join(str(x[0]) + "=" + str(x[1]) for x in zip(feature_names, fg))), file=sys.stderr)
                        array.append(fg)
                with open('features.txt', 'a+') as feats:
                    feats.write(str(array))

                if len(array) == 0:
                    orig_str_to_proba_comment_eid[orig_str].append((0.0, "missing_gene_position", eid))
                elif len(array) >= 100:
                    orig_str_to_proba_comment_eid[orig_str].append((1.0, "gt100_classifications", eid))
                else:
                    probas = classify.clf.predict_proba(array)[:, 1]
                    # print("variant %s, eid %s probabilities: %s" % (orig_str, eid, str(probas)), file=sys.stderr)
                    sys.stderr.flush()
                    if len(eids) == 1:
                        orig_str_to_proba_comment_eid[orig_str].append((1.0,
                                                                        "single_candidate=%f" % max(probas),
                                                                        eid))
                    else:
                        orig_str_to_proba_comment_eid[orig_str].append((max(probas), "classified=%f" % max(probas), eid))

    true_orig_str_eid = defaultdict(lambda: {})
    false_orig_str_eid = defaultdict(lambda: {})
    for orig_str, proba_comment_eid_list in orig_str_to_proba_comment_eid.items():
        proba_comment_eid_list = sorted(proba_comment_eid_list)[::-1]
        assert len(proba_comment_eid_list) >= 0
        classified_true = [proba_comment_eid for proba_comment_eid in proba_comment_eid_list
                           if proba_comment_eid[0] >= 0.5]  # magic number .5 gives us 95% recall hopefully
                                                            # .9 sometimes gives 90% recall
        classified_false = [proba_comment_eid for proba_comment_eid in proba_comment_eid_list
                           if proba_comment_eid[0] < 0.5]
        for proba_comment_eid in classified_true:
            true_orig_str_eid[orig_str][proba_comment_eid[2]] = (proba_comment_eid[0], proba_comment_eid[1])
        for proba_comment_eid in classified_false:
            false_orig_str_eid[orig_str][proba_comment_eid[2]] = (proba_comment_eid[0], proba_comment_eid[1])
    return true_orig_str_eid, false_orig_str_eid
classify.clf = None


def print_line_if_mapped(line, true_orig_str_to_eids, false_orig_str_to_eids, discarded_file):
    chrom, start, end, info, length, strand, \
        _, _, _, _, _, _, orig_variant_str, vcf_pos, \
        vcf_ref, vcf_alt, refseq_info = line
    info = info.split(',')
    eids = tuple(info[2].split('|'))
    for eid in eids:
        if eid in true_orig_str_to_eids[orig_variant_str]:
            print("%s\t%s" % ("\t".join(line), "\t".join(str(x) for x in true_orig_str_to_eids[orig_variant_str][eid])))
        elif eid in false_orig_str_to_eids[orig_variant_str]:
            print("%s\t%s" % ("\t".join(line), "\t".join(str(x) for x in false_orig_str_to_eids[orig_variant_str][eid])), file=discarded_file)
    sys.stdout.flush()


def main():
    in_filename = sys.argv[1]
    classifier_pkl_filename = sys.argv[2]
    discarded_filename = sys.argv[3]

    load_pmid_eid_to_right_gene_score()
    # get directly from file filtered_right_genes.tsv
    last_pmid = None
    last_pmid_eid_to_gposs = None
    last_pmid_orig_str_to_vposs = None
    last_pmid_orig_str_to_eids = None
    last_pmid_lines = []
    
    discarded_file = open(discarded_filename, 'w')
    for line in util.read_tsv(in_filename):
        chrom, start, end, info, length, strand, \
            _, _, _, _, _, _, orig_variant_str, vcf_pos, \
            vcf_ref, vcf_alt, refseq_info = line
        info = info.split(',')
        pmid = info[0]
        # this was commented out before:
        print("PMID %s, variant %s, (%s, %s, %s, %s)" % (pmid, orig_variant_str, chrom, start, end, info),
              file=sys.stderr)
        sys.stderr.flush()
        if pmid != last_pmid:
            if last_pmid_eid_to_gposs:
                true_orig_str_to_eids, false_orig_str_to_eids = classify(classifier_pkl_filename=classifier_pkl_filename,
                                            orig_str_to_vposs=last_pmid_orig_str_to_vposs,
                                            orig_str_to_eids=last_pmid_orig_str_to_eids,
                                            eids_to_gposs=last_pmid_eid_to_gposs,
                                            pmid=last_pmid)
                for last_pmid_line in last_pmid_lines:
                    print_line_if_mapped(last_pmid_line, true_orig_str_to_eids, false_orig_str_to_eids, discarded_file)
            last_pmid = pmid
            last_pmid_eid_to_gposs = defaultdict(lambda: set())
            last_pmid_orig_str_to_vposs = defaultdict(lambda: set())
            last_pmid_orig_str_to_eids = defaultdict(lambda: set())
            last_pmid_lines = []
        last_pmid_lines.append(line)
        eids = tuple(info[2].split('|'))
        refseq_id = info[6].split('.')[0]
        if orig_variant_str not in last_pmid_orig_str_to_vposs:
            variant_positions = get_pdf_positions(orig_variant_str, "variant_pos.tsv")
            last_pmid_orig_str_to_vposs[orig_variant_str] = variant_positions
        last_pmid_orig_str_to_eids[orig_variant_str].update(eids)
        gene_names = get_gene_names(pmid, eids, refseq_id)
        # eids <-> gene names is an intentional imprecision; 99.9% of all lines have only one eid anyways
        for eid in eids:
            for gene_name in gene_names:
                last_pmid_eid_to_gposs[eid].update(get_pdf_positions(gene_name, "gene_pos.tsv"))

    if last_pmid_eid_to_gposs:
        true_orig_str_to_eids, false_orig_str_to_eids = classify(classifier_pkl_filename=classifier_pkl_filename,
                                    orig_str_to_vposs=last_pmid_orig_str_to_vposs,
                                    orig_str_to_eids=last_pmid_orig_str_to_eids,
                                    eids_to_gposs=last_pmid_eid_to_gposs,
                                    pmid=pmid)
        for last_pmid_line in last_pmid_lines:
            print_line_if_mapped(last_pmid_line, true_orig_str_to_eids, false_orig_str_to_eids, discarded_file)

    discarded_file.close()


if __name__ == "__main__":
    main()
