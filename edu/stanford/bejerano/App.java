package edu.stanford.bejerano;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.pdfbox.pdmodel.PDDocument;

public class App {

    private static Map<String, Set<String>> parseVariants(String inTsv)
            throws IOException {
        BufferedReader reader = new BufferedReader(
                new InputStreamReader(new FileInputStream(new File(inTsv))));
        Map<String, Set<String>> pmidToSearchStrings = new HashMap<String, Set<String>>();
        String line;
        while ((line = reader.readLine()) != null) {
            String[] words = line.split("\t");
            String linePmid = words[3].split(",")[0];
            String origVariantString = words[12];
            String nmId = words[16].split(":")[0].split("\\.")[0];
            String seqId = words[3].split(",")[6].split("\\.")[0];
            if (!pmidToSearchStrings.containsKey(linePmid)) {
                pmidToSearchStrings.put(linePmid, new HashSet<String>());
            }
            pmidToSearchStrings.get(linePmid).add(origVariantString);
            pmidToSearchStrings.get(linePmid).add(nmId);
            pmidToSearchStrings.get(linePmid).add(seqId);
        }
        reader.close();
        return pmidToSearchStrings;
    }

    private static Map<String, Set<String>> parseGenes(String inTsv)
            throws IOException {
        Map<String, Set<String>> pmidToSearchStrings = new HashMap<String, Set<String>>();
        String line;
        String pmid = "curr";
        BufferedReader reader = new BufferedReader(
                new InputStreamReader(new FileInputStream(new File(inTsv))));
        while ((line = reader.readLine()) != null) {
            String[] words = line.split("\t");
            String[] origGeneNames = words[8].split("\\|\\^\\|");
            if (!pmidToSearchStrings.containsKey(pmid)) {
                pmidToSearchStrings.put(pmid, new HashSet<String>());
            }
            pmidToSearchStrings.get(pmid).addAll(Arrays.asList(origGeneNames));
        }
        reader.close();
        return pmidToSearchStrings;
    }

    public static void main(String[] args) throws IOException {
        String geneVsVariant = args[0];
        String inTsv = args[1];
        String pdf = args[2];
        String fontCacheDir = args[3];
        System.setProperty("pdfbox.fontcache", fontCacheDir);
        String pmid = "curr";
        Map<String, Set<String>> pmidToSearchStrings = null;
        if (geneVsVariant.equals("genes")) {
            pmidToSearchStrings = parseGenes(inTsv);
        } else if (geneVsVariant.equals("variants")) {
            pmidToSearchStrings = parseVariants(inTsv);
        } else {
            throw new IOException(
                    "Expected 'genes' or 'variants' in first argument");
        }
        PDDocument document = null;
        for (Entry<String, Set<String>> e : pmidToSearchStrings.entrySet()) {
            if (document != null)
                document.close();
            pmid = e.getKey();
            try {
                document = PDDocument
                        .load(new File(pdf));
            } catch (IOException ex) {
                System.err.println("Cannot load document " + pmid);
                ex.printStackTrace();
                continue;
            }
            TextLocator locator = new TextLocator(document);
            locator.setSortByPosition(false);
            List<PDFTextLocation> locations = locator.doSearch(e.getValue(),
                    pmid);
            for (PDFTextLocation location : locations) {
                if (location != null) {
                    // for some weird reason, very rarely PDFBox won't give me a
                    // location for some string ...
                    System.out.printf(
                            "%s\t%d\t%f\t%f\t%f\t%d\t%d\t%d\t%d\t%d\n",
                            location.getText(), location.getPage(),
                            location.getX(), location.getY(),
                            location.getOrientation(), location.getWordIndex(),
                            location.getNumStopwordsLeft(),
                            location.getNumStopwordsRight(),
                            location.getNumLetterLeft(),
                            location.getNumLetterRight());
                }
            }
        }
    }

}
