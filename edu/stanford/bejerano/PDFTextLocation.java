package edu.stanford.bejerano;

import java.util.List;
import java.util.Set;

public class PDFTextLocation implements Cloneable {

    private float x;
    private float y;
    private boolean found;
    private int page;
    private float orientation;
    private String text;
    private int wordIndex;
    private int numStopwordsLeft;
    private int numStopwordsRight;
    private int numLetterLeft;
    private int numLetterRight;

    public void setNumStopwordsLeft(List<String> allPdfTextList,
            Set<String> stopwords) {
        numStopwordsLeft = 0;
        for (int i = wordIndex - 20; i <= wordIndex - 1; i++) {
            if (i < 0)
                continue;
            if (stopwords.contains(allPdfTextList.get(i)))
                numStopwordsLeft++;
        }
    }

    public void setNumStopwordsRight(List<String> allPdfTextList,
            Set<String> stopwords) {
        numStopwordsRight = 0;
        for (int i = wordIndex + 1; i <= wordIndex + 20; i++) {
            if (i >= allPdfTextList.size())
                continue;
            if (stopwords.contains(allPdfTextList.get(i)))
                numStopwordsRight++;
        }
    }

    public void setNumLetterLeft(List<String> allPdfTextList) {
        numLetterLeft = 0;
        String leftString = "";
        for (int i = wordIndex - 20; i <= wordIndex - 1; i++) {
            if (i < 0)
                continue;
            String word = allPdfTextList.get(i);
            leftString += word;
        }
        for (int j = leftString.length() - 30; j < leftString.length(); j++) {
            if (j < 0)
                continue;
            char ch = leftString.charAt(j);
            if (Character.isLetter(ch))
                numLetterLeft++;
        }
    }

    public void setNumLetterRight(List<String> allPdfTextList) {
        numLetterRight = 0;
        String rightString = "";
        for (int i = wordIndex + 1; i <= wordIndex + 20; i++) {
            if (i >= allPdfTextList.size())
                continue;
            String word = allPdfTextList.get(i);
            rightString += word;
        }
        for (int j = 0; j < 30; j++) {
            if (j >= rightString.length())
                break;
            char ch = rightString.charAt(j);
            if (Character.isLetter(ch))
                numLetterRight++;
        }
    }

    public int getNumLetterLeft() {
        return numLetterLeft;
    }

    public int getNumLetterRight() {
        return numLetterRight;
    }

    public int getNumStopwordsLeft() {
        return numStopwordsLeft;
    }

    public int getNumStopwordsRight() {
        return numStopwordsRight;
    }

    public int getWordIndex() {
        return wordIndex;
    }

    public void setWordIndex(int wordIndex) {
        this.wordIndex = wordIndex;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public boolean isFound() {
        return found;
    }

    public void setFound(boolean found) {
        this.found = found;
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public float getOrientation() {
        return orientation;
    }

    public void setOrientation(float orientation) {
        this.orientation = orientation;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

}
