#! /usr/bin/env python3

# from __future__ import print_function
import sys
import os
# import io

import text.tuples as tuples
from text import extractor_util
from text.right_gene.extract_right_gene import extract_right_gene_proba
#from text.text_database import ReadDatabase, WriteDatabase, NotFoundException


def perform(pkl_suffix, current_pmid, full_genes, title_genes, abstract_genes, out_db):
    try:
        full_gene_mentions = tuples.read_text(full_genes, tuples.geneMentionParser,
                                              tuples.GeneMention, ignoreTypeErrors=False)
    except Exception as e:
        print(e)

    try:
        title_gene_mentions = tuples.read_text(title_genes, tuples.geneMentionParser,
                                                   tuples.GeneMention, ignoreTypeErrors=False)    
    except:
        title_gene_mentions = ""

    try:
        abstract_gene_mentions = tuples.read_text(abstract_genes, tuples.geneMentionParser,
                                                   tuples.GeneMention, ignoreTypeErrors=False)
    except:
        abstract_gene_mentions = ""

    the_genes = extract_right_gene_proba(pkl_suffix, current_pmid, title_gene_mentions,
                                        abstract_gene_mentions, full_gene_mentions)
    for g in the_genes:
        out_db.write(extractor_util.get_tsv_output(g) + "\n")


def main(pkl_suffix, out_basedir, out_name):
    gene_text = ""
    with open(os.path.join(os.path.dirname(__file__), 'candidate_genes.tsv'), 'rt') as genes,\
            open(os.path.join(os.path.dirname(__file__), 'title_genes.tsv'), 'rt') as title_genes,\
            open(os.path.join(os.path.dirname(__file__), 'abstract_genes.tsv'), 'rt') as abstract_genes:
        with open(os.path.join(out_basedir, out_name), 'w+') as out_db:
        
            gene_text = genes.readlines()
            title_gene_text = title_genes.readlines()
            abstract_gene_text = abstract_genes.readlines()
           
            perform(pkl_suffix, "", gene_text, title_gene_text, abstract_gene_text, out_db)


if __name__ == "__main__":
    main(sys.arg[1], sys.arg[2], sys.arg[3])
