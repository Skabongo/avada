from collections import namedtuple
import gzip
import re
import collections
from collections import defaultdict


def iter_tsv_rows(filename):
    row = None
    with open(filename) as f:
        for i, line in enumerate(f):
            line = line.rstrip('\n')
            if i == 0:
                assert line.startswith('#')
                line = line.lstrip('#')
                line = line.split('\t')
                row = namedtuple('Row', line)
            else:
                assert row is not None
                yield row(*line.split('\t'))


def read_tsv(filename, skip_lines=0, stop_at_empty=False, skip_if_startswith=None):
    with open(filename) as f:
        for i, line in enumerate(f):
            if skip_if_startswith and line.startswith(skip_if_startswith):
                continue
            if stop_at_empty and len(line.strip()) == 0:
                break
            if i >= skip_lines:
                yield line.rstrip('\n').split('\t')


def tail(filename, skip_lines=0, stop_at_empty=False, stop_at_line=None, skip_if_startswith=None):
    with open(filename) as f:
        for i, line in enumerate(f):
            if skip_if_startswith and line.startswith(skip_if_startswith):
                continue
            if stop_at_empty and len(line.strip()) == 0:
                break
            if stop_at_line and line.startswith(stop_at_line):
                break
            if i >= skip_lines:
                yield line.rstrip('\n')


def max_iter_tsv_rows(inFile, headers=None, fmt=None, noHeaderCount=None, fieldTypes=None, encoding="utf8",
                fieldSep="\t", isGzip=False, skipLines=None, makeHeadersUnique=False, comment_prefix=None):
    # MAX HAEUSSLER
    """ 
        parses tab-sep file with headers as field names 
        yields collection.namedtuples
        strips "#"-prefix from header line

        if file has no headers: 
        
        a) needs to be called with 
        noHeaderCount set to number of columns.
        headers will then be named col0, col1, col2, col3, etc...

        b) you can also set headers to a list of strings
        and supply header names in that way.
    
        c) set the "fmt" to one of: psl, this will also do type conversion

        fieldTypes can be a list of types.xxx objects that will be used for type
        conversion (e.g. types.    int)

        - makeHeadersUnique will append _1, _2, etc to make duplicated headers unique.
        - skipLines can be used to skip x lines at the beginning of the file.
        - if encoding is None file will be read as byte strings.
        - comment_prefix specifies a character like "#" that markes lines to skip
    """

    if noHeaderCount:
        numbers = range(0, noHeaderCount)
        headers = ["col" + str(x) for x in numbers]

    if fmt == "psl":
        headers = ["score", "misMatches", "repMatches", "nCount", "qNumInsert", "qBaseInsert",
                   "tNumInsert", "tBaseInsert", "strand", "qName", "qSize", "qStart", "qEnd",
                   "tName", "tSize", "tStart", "tEnd", "blockCount", "blockSizes", "qStarts", "tStarts"]
        fieldTypes = [int, int, int, int, int, int, int, int, str, str, int,
                          int, int, str, int, int, int, int , str, str, str]
    elif fmt == "bed12":
        headers = ["chrom", "chromStart", "chromEnd", "name", "score", "strand",
                   "thickStart", "thickEnd", "itemRgb", "blockCount", "blockSizes", "blockStarts"]
        fieldTypes = [str, int, int, str, int, str, int, int, str, int, str, str]

    if isinstance(inFile, str):
        if inFile.endswith(".gz") or isGzip:
            # zf = gzip.open(inFile, 'rb')
            fh = gzip.open(inFile, 'rb')
            # reader = codecs.getreader(encoding)
            # fh = reader(zf)
        else:
            fh = open(inFile)
            # if encoding!=None:
                # fh = codecs.open(inFile, encoding=encoding)
            # else:
                # fh = open(inFile)
    else:
        fh = inFile

    if headers == None:
        line1 = fh.readline()
        line1 = line1.rstrip("\n").strip("#").rstrip("\t")
        headers = line1.split(fieldSep)
        headers = [h.strip() for h in headers]
        headers = [re.sub("[^a-zA-Z0-9_]", "_", h) for h in headers]
        newHeaders = []
        for h in headers:
            if h[0].isdigit():
                h = "n" + h
            newHeaders.append(h)
        headers = newHeaders

    if makeHeadersUnique:
        newHeaders = []
        headerNum = defaultdict(int)
        for h in headers:
            headerNum[h] += 1
            if headerNum[h] != 1:
                h = h + "_" + str(headerNum[h])
            newHeaders.append(h)
        headers = newHeaders

    if skipLines:
        for i in range(0, skipLines):
            fh.readline()

    Record = collections.namedtuple('tsvRec', headers)
    for line in fh:
        if comment_prefix is not None and line.startswith(comment_prefix):
            continue
        line = line.strip("\n")
        fields = line.split(fieldSep)
        if encoding is not None:
            fields = [str(f) for f in fields]
        # fields = [x.decode(encoding) for x in fields]
        if fieldTypes:
            fields = [f(x) for f, x in zip(fieldTypes, fields)]
        try:
            rec = Record(*fields)
        except:
            raise Exception("wrong field count in line %s" % line)
        # convert fields to correct data type
        yield rec
